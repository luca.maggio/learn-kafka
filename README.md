# FirstKafka

FirstKafka is a basically approach to write a simple consumer that print a message from a kafka queue.

## Start

```bash
make
``` 
or

```bash
mvn clean install && mvn spring-boot:run
```

## Usage

* Start your `zookeeper-server`:

```bash
./bin/zookeeper-server-start.sh config/zookeeper.properties
```

* Start your `kafka-server`:

```bash
./bin/kafka-server-start.sh config/server.properties
```

* Start your `kafka-console-producer`:

```bash
./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic NewTopic
```

* Start the application:

```bash
make
```

or

```bash
mvn clean install && mvn spring-boot:run
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
