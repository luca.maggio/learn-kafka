package com.kafkafirstexample.kafkafirst.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

  @KafkaListener(topics = "NewTopic", groupId = "groupId")
  public void consume(String message) {
    System.out.println("message = " + message);
  }
}
